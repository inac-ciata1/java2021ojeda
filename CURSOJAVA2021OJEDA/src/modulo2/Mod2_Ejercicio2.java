package modulo2;
public class Mod2_Ejercicio2 {
	public static void main(String[] args) {
		byte 		bmin = -128;
		byte 		bmax = 127;
		System.out.println("tipo\tminimo\tmaximo");
		System.out.println("....\t......\t......");
		System.out.println("\nbyte\t" + bmin + "\t" + bmax);
		short 	smin = -32768;
		short 	smax = 32767;
		System.out.println("\nshort\t" + smin + "\t" + smax);
		int 		imax = -2147483648;
		int 		imin = 2147483647;
		System.out.println("\nint\t" + imin + "\t" + imax);
		long 		lmin = -9223372036854775808L;
		long 		lmax = 9223372036854775807L;
		System.out.println("\nlong\t" + lmin + "\t" + lmax);
	
//La formula que podr�a utilizarse ser�a m�ximo=(total/2)-1; m�nimo=total/(-2)

	}

}
