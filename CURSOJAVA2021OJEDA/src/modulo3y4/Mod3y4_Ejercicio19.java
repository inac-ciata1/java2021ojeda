package modulo3y4;

public class Mod3y4_Ejercicio19 {
	// Realizar un ciclo while que muestre 10 nmeros al azar informando su suma y su promedio.
	
	public static void main(String[] args) {
		System.out.println("Nmeros al azar:");
		
		int numero;
		double promedio;
		int suma=0;
		int c=1;
		
		while(c<11){
			c++;
			numero=(int)(Math.random()*100);
			suma+=numero;
			System.out.println(numero);
		}
		
		promedio=(double)suma/10;
		
		System.out.println("\nSuma = " + suma);
		System.out.println("Promedio = " + promedio);
	}


}
