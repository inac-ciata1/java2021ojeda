package modulo3y4;
import java.util.Scanner;
public class Mod3y4_Ejercicio15 {
	/* Realizar un sistema que permita mostrar las caracter�sticas de un auto utilizando 
	 * para ello una variable de tipo char, esta se llenara con los valores �a�, �b� o �c�, 
	 * siendo de clase �a� los que tienen 4 ruedas y un motor, clase �b� 4 ruedas,  un motor, 
	 * cierre centralizado y aire, y clase �c� 4 ruedas,  un motor, cierre centralizado, 
	 * aire, airbag. */

	
	public static void main(String[] args) {
		Scanner ingreso = new Scanner(System.in);
		
		System.out.print("Introduzca la categor�a del auto ---> ");
		char categoria = ingreso.next().charAt(0);
		
		switch(categoria){
		 case 'a':
			 System.out.println("El auto cuenta con cuatro ruedas y un motor.");
			 break;
		 case 'b':
			 System.out.println("El auto cuenta con cuatro ruedas, un motor, cierre centralizado y aire.");
			 break;
		 case 'c':
			 System.out.println("El auto cuenta con cuatro ruedas, un motor, cierre centralizado, aire y airbag.");
			 break;
		 default:
				System.out.println("ERROR: Categor�a incorrecta.");
		}
		
		ingreso.close();
	}

}
