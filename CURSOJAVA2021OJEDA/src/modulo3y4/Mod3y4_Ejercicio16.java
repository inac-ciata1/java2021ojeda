package modulo3y4;
import java.util.Scanner;
public class Mod3y4_Ejercicio16 {
	/* Realizar un programa que permita mostrar en pantalla la tabla de multiplicar 
	 * de un valor ingresado a travs de una variable. */
	
	public static void main(String[] args) {
		Scanner ingreso = new Scanner(System.in);
		
		System.out.print("Ingrese un nmero ---> ");
		int n = ingreso.nextInt();
		
		System.out.println("Tabla de multiplicar del " + n + ":");
		
		for(int i=0;i<11;i++){
			System.out.println(n + " x " + i + " = " + n*i);
		}
		
		ingreso.close();
	}

}
