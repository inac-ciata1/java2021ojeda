package modulo3y4;
import java.util.Scanner;
public class Mod3y4_Ejercicio14 {
	public static void main(String[] args) {
		/* Elaborar un sistema que permita realizar el ejercicio
		 * numero 5 a utilizando esta vez la sentencia switch. */

		
		Scanner ingreso = new Scanner(System.in);
		
		System.out.print("Introduzca el puesto del competidor ---> ");
		int pos = ingreso.nextInt();
		
		switch(pos){
			case 1:
				System.out.println("El competidor obtiene la medalla de oro.");
				break;
			case 2:
				System.out.println("El competidor obtiene la medalla de plata.");
				break;
			case 3:
				System.out.println("El competidor obtiene la medalla de bronce.");
				break;
			default:
				System.out.println("Siga participando.");
		}
		
		ingreso.close();
	}

}
