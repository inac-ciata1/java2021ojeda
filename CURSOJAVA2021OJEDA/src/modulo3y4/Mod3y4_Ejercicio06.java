package modulo3y4;
import java.util.Scanner;
public class Mod3y4_Ejercicio06 {
	/* Realizar un sistema que permita determinar a trav�s de una variable de tipo int 
	 * correspondiente al curso al que pertenece seg�n el siguiente criterio:
	 * a. 0 debe mostrarse en pantalla el texto �jard�n de infantes�.
	 * b. 1 y 6 se mostrara en pantalla el texto �primaria�.
	 * c. 7 y 12 se mostrara el texto �secundaria�. */

	
	public static void main(String[] args) {
		Scanner ingreso = new Scanner(System.in);
		
		System.out.print("Ingrese el curso ---> ");
		int curso = ingreso.nextInt();
		
		if(curso==0){
			System.out.println("jard�n de infantes");
		}
		else if(curso>=1 && curso<=6){
			System.out.println("primaria");
		}
		else if(curso>=7 && curso<=12){
			System.out.println("secundaria");
		}
		else{
			System.out.println("curso incorrecto");
		}
		
		ingreso.close();
	}

}
