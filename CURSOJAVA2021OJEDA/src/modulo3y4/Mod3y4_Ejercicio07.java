package modulo3y4;
import java.util.Scanner;
public class Mod3y4_Ejercicio07 {
	/* Realizar un programa que permita la definición de 3 variables de tipo entera 
	 * e imprimir la mayor de todas. */

	
	public static void main(String[] args) {
		Scanner ingreso = new Scanner(System.in);
		
		System.out.print("Ingrese el primer valor ---> ");
		int a = ingreso.nextInt();
		
		System.out.print("Ingrese el segundo valor ---> ");
		int b = ingreso.nextInt();
		
		System.out.print("Ingrese el tercer valor ---> ");
		int c = ingreso.nextInt();
		
		System.out.print("El mayor valor es ");
		
		if(a>b){
			if(a>c){
				System.out.println(a);
			}
			else{
				System.out.println(c);
			}
		}
		else{
			if(b>c){
				System.out.println(b);
			}
			else{
				System.out.println(c);
			}
		}
		
		ingreso.close();
	}

}
