package modulo3y4;
import java.util.Scanner;
public class Mod3y4_Ejercicio02 {

		public static void main (String[] args){
			Scanner ingreso = new Scanner(System.in);
			
			System.out.print("Ingrese un n�mero ---> ");
			int numero=ingreso.nextInt();
			
			if(numero%2==0){
				System.out.println("El n�mero " + numero + " es par.");
			}
			else{
				System.out.println("El n�mero " + numero + " es impar.");
			}
			
			ingreso.close();
		}
	}