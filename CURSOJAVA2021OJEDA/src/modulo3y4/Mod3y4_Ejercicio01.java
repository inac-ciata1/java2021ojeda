package modulo3y4;

import java.util.Scanner;
public class Mod3y4_Ejercicio01 {
	

		
		public static void main (String[] args){
			Scanner ingreso=new Scanner(System.in);
			
			System.out.print("Ingrese la nota del primer trimestre ---> ");
			float primer_trimestre = ingreso.nextFloat();
			
			System.out.print("Ingrese la nota del segundo trimestre ---> ");
			float segundo_trimestre = ingreso.nextFloat();
			
			System.out.print("Ingrese la nota del tercer trimestre ---> ");
			float tercer_trimestre = ingreso.nextFloat();
			
			float promedio = (primer_trimestre + segundo_trimestre + tercer_trimestre)/3;
			
			if(promedio>=7){
				System.out.println("El alumno aprob� con un promedio de " + promedio);
			}
			else {
				System.out.println("El alumno reprob� con un promedio de " + promedio);
			}
			
			ingreso.close();
		}
	}