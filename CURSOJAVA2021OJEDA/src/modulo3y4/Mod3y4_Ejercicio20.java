package modulo3y4;

public class Mod3y4_Ejercicio20 {
	/* 20.	Realizar un ciclo do while que muestre 10 nmeros al azar (Math.random() )
	 * informando el mximo y el mnimo de ellos. */

		public static void main(String[] args) {
			System.out.println("Nmeros al azar:");
			
			int numero;
			int minimo=0;
			int maximo=0;
			int c=1;
			
			do{
				numero=(int)(Math.random()*100);
				if(c==1){
					minimo=numero;
					maximo=numero;
				}
				else if(numero>maximo){
					maximo=numero;
				}
				else if(numero<minimo){
					minimo=numero;
				}
				
				System.out.println(numero);
				c++;	
			} while(c<11);
			
			System.out.println("\nNmero mximo = " + maximo);
			System.out.println("Nmero mnimo = " + minimo);
		}


	}