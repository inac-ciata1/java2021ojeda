package modulo3y4;

public class Mod3y4_Ejercicio22 {
	/* 22.	(solo para valientes), tomar el ejercicio anterior y realizar un ciclo que se repita 
	 * 10 veces tomando al categoras al azar, lo mismo que el suelto con valores lgicos.
	 * a. Utilizar Math.random(), y , utilizar un ciclo while y solamente salir de este cuando el 
	 * resultado este dentro de los correctos. */

	public static void main(String[] args) {
		char categoria;
		int ant;
		float sueldo;
		
		float multiplicador=1;
		float sueldoNeto;
		
		System.out.println("Categor�a\tAntigedad\tSueldo base\t\tSueldo final");
		
		int i=1;
		while(i<11){
			categoria = (char)(int)(Math.random()*3+65);
			ant = (int)(Math.random()*30);
			sueldo = (float)Math.random()*90000+10000;
			
			if(ant>0 && ant<6){
				multiplicador=1.05F;
			}
			else if(ant>5 && ant<11){
				multiplicador=1.1F;
			}
			else if(ant>10){
				multiplicador=1.3F;
			}
			
			sueldoNeto = sueldo*multiplicador;
			
			switch(categoria){
			case 'A':
				sueldoNeto += 1000;
				break;
			case 'B':
				sueldoNeto += 2000;
				break;
			case 'C':
				sueldoNeto += 3000;
				break;
			}
			
			System.out.println(categoria + "\t\t" + ant + "\t\t" + sueldo + "\t\t" + sueldoNeto);
			i++;
		}
		
	}

}
