package modulo3y4;
import java.util.Scanner;
public class Mod3y4_Ejercicio08 {
	/* Realizar un programa que permita tener dos variables de tipo int, si se 
	 * define 0 como piedra, 1 como papel y 2 como tijera, teniendo 2 competidores, 
	 * determinar cual de ellos es el ganador. */


		public static void main(String[] args) {
			Scanner ingreso = new Scanner(System.in);
			
			System.out.println("�Piedra (0), papel (1) o tijera (2)!");
			System.out.print("Ingrese la jugada del jugador 1 ---> ");
			int comp1 = ingreso.nextInt();
			
			System.out.print("Ingrese la jugada del jugador 2 ---> ");
			int comp2 = ingreso.nextInt();
			
			if(comp1<0 || comp1>2 || comp2<0 || comp2>2){
				System.out.println("ERROR: Jugada incorrecta.");
			}
			else if(comp1==comp2){
				System.out.println("�Empate!");
			}
			else{
				if(comp1==0){	//Piedra
					if(comp2==1){	//Papel
						System.out.println("�Gana el jugador 2!");
					}
					else{	//Tijera
						System.out.println("�Gana el jugador 1!");
					}
				}
				else if(comp1==1){ //Papel
					if(comp2==0){	//Piedra
						System.out.println("�Gana el jugador 1!");
					}
					else{	//Tijera
						System.out.println("�Gana el jugador 2!");
					}
				}
				else{	//Tijera
					if(comp2==0){	//Piedra
						System.out.println("�Gana el jugador 2!");
					}
					else{	//Papel
						System.out.println("�Gana el jugador 1!");
					}
				}
			}
			
			ingreso.close();
		}
}



