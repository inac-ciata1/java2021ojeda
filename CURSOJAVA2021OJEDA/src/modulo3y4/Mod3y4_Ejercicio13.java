package modulo3y4;
import java.util.Scanner;
public class Mod3y4_Ejercicio13 {
	// Elaborar un sistema que permita realizar el ejercicio numero 3 a utilizando esta vez la sentencia switch.
	
	
	public static void main(String[] args) {
		Scanner ingreso = new Scanner(System.in);
		
		System.out.print("Ingrese un mes ---> ");
		String mes=ingreso.nextLine();
		
		switch(mes){
			case "Enero":
				System.out.println(mes + " tiene 31 das.");
				break;
			case "enero":
				System.out.println(mes + " tiene 31 das.");
				break;
			case "Febrero":
				System.out.println(mes + " tiene 28 das.");
				break;
			case "febrero":
				System.out.println(mes + " tiene 28 das.");
				break;
			case "Marzo":
				System.out.println(mes + " tiene 31 das.");
				break;
			case "marzo":
				System.out.println(mes + " tiene 31 das.");
				break;
			case "Abril":
				System.out.println(mes + " tiene 30 das.");
				break;
			case "abril":
				System.out.println(mes + " tiene 30 das.");
				break;
			case "Mayo":
				System.out.println(mes + " tiene 31 das.");
				break;
			case "mayo":
				System.out.println(mes + " tiene 31 das.");
				break;
			case "Junio":
				System.out.println(mes + " tiene 30 das.");
				break;
			case "junio":
				System.out.println(mes + " tiene 30 das.");
				break;
			case "Julio":
				System.out.println(mes + " tiene 31 das.");
				break;
			case "julio":
				System.out.println(mes + " tiene 31 das.");
				break;
			case "Agosto":
				System.out.println(mes + " tiene 31 das.");
				break;
			case "agosto":
				System.out.println(mes + " tiene 31 das.");
				break;
			case "Septiembre":
				System.out.println(mes + " tiene 30 das.");
				break;
			case "septiembre":
				System.out.println(mes + " tiene 30 das.");
				break;
			case "Setiembre":
				System.out.println(mes + " tiene 30 das.");
				break;
			case "setiembre":
				System.out.println(mes + " tiene 30 das.");
				break;
			case "Octubre":
				System.out.println(mes + " tiene 31 das.");
				break;
			case "octubre":
				System.out.println(mes + " tiene 31 das.");
				break;
			case "Noviembre":
				System.out.println(mes + " tiene 30 das.");
				break;
			case "noviembre":
				System.out.println(mes + " tiene 30 das.");
				break;
			case "Diciembre":
				System.out.println(mes + " tiene 31 das.");
				break;
			case "diciembre":
				System.out.println(mes + " tiene 31 das.");
				break;
			default:
				System.out.println(mes + " no es un mes.");
		}
		
		ingreso.close();
	}

}
