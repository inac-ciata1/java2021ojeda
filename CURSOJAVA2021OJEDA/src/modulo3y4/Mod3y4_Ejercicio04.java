package modulo3y4;
import java.util.Scanner;
public class Mod3y4_Ejercicio04 {
	//* Realizar un programa que permita ingresar una categor�a las cuales pueden ser  �a�, �b� o �c�, 
	 //* a. Si corresponde a esta categor�a mostrar en pantalla la palabra �hijo�
	 //* b. Si  corresponde a esta categor�a mostrar en pantalla la palabra �padres�
	 //* c. Si  corresponde a esta categor�a mostrar en pantalla la palabra �abuelos�. *

		public static void main(String[] args) {
			Scanner ingreso=new Scanner(System.in);
			
			System.out.print("Ingrese una categor�a ---> ");
			char mes=ingreso.next().charAt(0);
			
			if(mes=='a'){
				System.out.println("hijo");
			}
			else if(mes=='b'){
				System.out.println("padres");
			}
			else if(mes=='c'){
				System.out.println("abuelos");
			}
			else{
				System.out.println("Categor�a incorrecta");
			}
			
			ingreso.close();
		}
}


