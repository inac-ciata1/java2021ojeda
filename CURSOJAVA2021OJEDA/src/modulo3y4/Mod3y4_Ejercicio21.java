package modulo3y4;
import java.util.Scanner;
public class Mod3y4_Ejercicio21 {
	/* De un empleado se conoce 
	 * a. la categor�a: A,B,C
	 * b. la antig�edad
	 * c. el sueldo.
	 * Se quiere determinar el sueldo neto sabiendo que si la antig�edad esta entre
	 * 1 y 5 a�os se le aumentan un 5% al sueldo bruto.
	 * 6 y 10 a�os se le aumenta un 10% al sueldo bruto.
	 * Mas de 10 un 30%
	 * Y un plus por categor�a
	 * A= 1000, B=2000, c=3000 */

	public static void main(String[] args) {
		Scanner ingreso = new Scanner(System.in);
		
		System.out.print("Ingrese la categor�a del empleado ---> ");
		char categoria = ingreso.next().charAt(0);
		System.out.print("Ingrese su antig�edad (a�os) ---> ");
		int ant = ingreso.nextInt();
		System.out.print("Ingrese su sueldo bruto ---> ");
		double sueldo = ingreso.nextDouble();
		
		double multiplicador=1;
		double sueldoNeto;
		
		if(categoria!='A' && categoria!='B' && categoria!='C'){
			System.out.println("ERROR: Categor�a incorrecta.");
		}
		else if(ant<0){
			System.out.println("ERROR: Antig�edad incorrecta.");
		}
		else if(sueldo<0){
			System.out.println("ERROR: Sueldo incorrecto.");
		}
		else{
			if(ant>0 && ant<6){
				multiplicador=1.05;
			}
			else if(ant>5 && ant<11){
				multiplicador=1.1;
			}
			else if(ant>10){
				multiplicador=1.3;
			}
			
			sueldoNeto = sueldo*multiplicador;
			
			switch(categoria){
				case 'A':
					sueldoNeto += 1000;
					break;
				case 'B':
					sueldoNeto += 2000;
					break;
				case 'C':
					sueldoNeto += 3000;
					break;
			}
			
			System.out.println("El sueldo neto del empleado es " + sueldoNeto);
		}
		
		ingreso.close();
	}

}
